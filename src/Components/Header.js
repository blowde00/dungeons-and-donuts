import React from "react";
import "./Styles/headerfooter.css";
import { Navbar, Container, Nav } from "react-bootstrap";
import { Link } from 'react-router-dom';

export default function Header() {
  return (
    <>
      <div className="donutHeader">
        <img
          id="donutPic"
          src={require("./Images/donut_112x112.png")}
          alt="dungeonAndDonutsLogo"
        />
        <h1>Dungeons & Donuts</h1>
      </div>

      <Navbar expand="lg">
        <Container>
          <Navbar.Brand href="#home"></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Link className="footerLinks" to='/'>Home</Link>
              <Link className="footerLinks" to='/newplayer'>New Player</Link>
              <Link className="footerLinks" to='/generator'>Character Generator</Link>
              <Link className="footerLinks" to='/resources'>Resources</Link>
              {/* <Nav.Link href="/">Home</Nav.Link>
              <Nav.Link href="/newplayer">New Player</Nav.Link>
              <Nav.Link href="/generator">Character Generator</Nav.Link>
              <Nav.Link href="/resources">Resources</Nav.Link> */}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
