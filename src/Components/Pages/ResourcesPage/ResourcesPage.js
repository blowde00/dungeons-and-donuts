import React, { useEffect, useReducer } from 'react';
import { Accordion } from 'react-bootstrap';
import Rule from './Rule';
import RuleContext from '../../../Context/RuleContext';

const rulesURL = 'https://www.dnd5eapi.co/api/rule-sections/';

const initialRuleState = {
  rules: []
}

const ruleReducer = (state, action) => {
  switch (action.type) {
    case 'rulesLoad':
      return { ...state, rules: action.payload }
    default:
      return
  }
}

export default function ResourcesPage() {
  const [state, dispatch] = useReducer(ruleReducer, initialRuleState)

  useEffect(() => {
    fetch(rulesURL).then((response) => {
      return response.json();
    })
      .then((data) => {
        dispatch({ type: 'rulesLoad', payload: data.results })
      })
  }, [])

  return (
    <RuleContext.Provider value={[state, rulesURL]}>
      <div className="spacing">
        <h2 className='firstHeading'>Resources</h2>
        <p>This page provides the complete documentation on the rules of Dungeons & Dragons. Each section provides information on the different mechanics of playing the game. This page is intended to be used as a quick reference guide. No need to memorize all of this information, and be sure to ask your Dungeon Master if you have any questions!</p>
        <Accordion className="resourcesAccordion">
          {state.rules.map(rule => {
            return (
              <Accordion.Item key={rule.index} eventKey={rule.index}>
                <Accordion.Header>
                  {rule.name}
                </Accordion.Header>
                <Accordion.Body>
                  <Rule rule={rule} />
                </Accordion.Body>
              </Accordion.Item>
            )
          })}
        </Accordion>
      </div>
    </RuleContext.Provider>
  )
}
