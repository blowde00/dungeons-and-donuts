import React, { useState } from 'react';
import { useContext } from 'react';
import RuleContext from '../../../Context/RuleContext';
import ReactMarkdown from 'react-markdown'

export default function Rule({ rule }) {
  const [state, rulesURL] = useContext(RuleContext)
  const [ruleDesc, setRuleDesc] = useState()

  fetch(rulesURL + rule.index).then((response) => {
    return response.json();
  })
    .then((data) => {
      setRuleDesc(data.desc);
    })

  return (
    <ReactMarkdown>{ruleDesc}</ReactMarkdown>
  )
}
