import React from 'react';
import { useContext, useState, useEffect } from 'react';
import GenContext from '../../../Context/GenContext';

export default function Score({ result }) {
  const [state, dispatch, abilityURL, Form] = useContext(GenContext)
  const [scoreInput, setScoreInput] = useState(10)
  const [scoreMod, setScoreMod] = useState(0)

  useEffect(() => {
    saveAbilityChange()
  }, [])

  const onAbilityChange = (e) => {
    setScoreInput(e.target.value);
    let scoreModifier = Math.floor((e.target.value - 10) / 2);
    setScoreMod(scoreModifier);
  }

  const saveAbilityChange = () => {
    let scoreObject = { ...result };
    scoreObject.value = scoreInput;
    scoreObject.mod = scoreMod;
    dispatch({ type: 'addScore', payload: scoreObject })
  }

  return (
    <>
      <Form.Control disabled value={scoreInput} />
      <Form.Range onChange={onAbilityChange} onMouseOut={saveAbilityChange}
        type="number" min='1' max='20' value={scoreInput}>
      </Form.Range>
    </>
  )
}
