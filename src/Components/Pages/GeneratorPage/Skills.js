import React, { useContext, useEffect } from 'react';
import { Container, Form, Row, Col } from 'react-bootstrap';
import GenContext from '../../../Context/GenContext';
import SkillMod from './SkillMod';

const skillsURL = 'https://www.dnd5eapi.co/api/skills/';

export default function Skills() {
  const [state, dispatch] = useContext(GenContext)

  useEffect(() => {
    fetch(skillsURL).then((response) => {
      return response.json();
    })
      .then((data) => {
        dispatch({ type: 'skillsLoad', payload: data.results })
      })
  }, [dispatch])

  return (
    <Container>
    <Form>
      <Row>
      {state.abilityResults.length > 0 ? state.skillResults.map(skill => {
        return (
          <Col key={skill.index} className="skillColumn" xs={4}>
          <Form.Group key={skill.index}>
            <Form.Label htmlFor={skill.index} >
              {skill.name}
            </Form.Label>
            <SkillMod skill={skill} skillsURL={skillsURL} />
          </Form.Group>
          </Col>
        )
      }) : 'Loading Skills'}
      
      </Row>
    </Form>
    </Container>
  )
}

