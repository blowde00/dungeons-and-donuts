import React, { useState } from 'react'
import { Form } from 'react-bootstrap';
import ClassesData from "../../Data/ClassesData";

export default function ClassSelect() {
  const [classes, setClasses] = useState("")

  const classDataList = ClassesData

  function onSelectClassSelector(eventKey) {
    let classList = classDataList.map((chosenClass) => {
      if (chosenClass.id === eventKey) {
        setClasses(classList);
      }
      return classes;
    });
  }

  return (
    <Form.Select aria-label="Default select example">
      <option onClick={onSelectClassSelector} hidden defaultValue>Choose your Class</option>
      {classDataList.map(classes => {
        return (
          <option key={classes.id}>{classes.id}</option>
        )
      })}
    </Form.Select>
  )
}