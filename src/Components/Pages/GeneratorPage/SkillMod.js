import React, { useContext, useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import GenContext from '../../../Context/GenContext';

export default function SkillMod({ skill, skillsURL }) {
  const [state, dispatch] = useContext(GenContext)
  const [skillMod, setSkillMod] = useState("")
  const [fMod, setFMod] = useState('')

  useEffect(() => {
    fetch(skillsURL + skill.index).then((response) => {
      return response.json();
    })
      .then((data) => {
        let scoreMod = data.ability_score.index
        let finalMod = state.abilityResults.find(skill => skill.index === scoreMod)
        setSkillMod(scoreMod)
        setFMod(finalMod)
      })
  }, [state.abilityResults])

  return (
    <>
      <Form.Control title={`Changing ${fMod.name} updates this skill`}   disabled value={fMod ? fMod.mod : 0} />
    </>
  )
}