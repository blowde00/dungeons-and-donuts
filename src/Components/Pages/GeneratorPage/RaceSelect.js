import React, { useState } from 'react'
import { Form } from 'react-bootstrap';
import RacesData from "../../Data/RacesData";

export default function RaceSelect() {
  const [races, setRaces] = useState("")

  const racesDataList = RacesData

  function onSelectRaceSelector(eventKey) {
    let raceList = racesDataList.map((chosenRace) => {
      if (chosenRace.id === eventKey) {
        setRaces(raceList);
      }
      return races;
    });
  }

  return (
    <Form.Select aria-label="Default select example">
      <option onClick={onSelectRaceSelector} hidden defaultValue>Choose your Race</option>
      {racesDataList.map(race => {
        return (
          <option key={race.id}>{race.id}</option>
        )
      })}
    </Form.Select>
  )
}