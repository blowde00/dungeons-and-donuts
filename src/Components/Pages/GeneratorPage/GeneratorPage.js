import React, { useEffect, useReducer } from "react";
import { Form, Container, Row, Col } from "react-bootstrap";
import Score from "./Score";
import GenContext from "../../../Context/GenContext";
import Skills from "./Skills";
import RaceSelect from "./RaceSelect";
import ClassSelect from "./ClassSelect";

const abilityURL = "https://www.dnd5eapi.co/api/ability-scores";

const initialGenState = {
  abilityResults: [],
  skillResults: [],
};

const genReducer = (state, action) => {
  switch (action.type) {
    case "genLoad":
      return { ...state, abilityResults: action.payload };
    case "skillsLoad":
      return { ...state, skillResults: action.payload };
    case "addScore":
      let newAbilityResults = [...state.abilityResults];
      let foundResult = newAbilityResults.find(
        (result) => result.index === action.payload.index
      );
      foundResult.value = action.payload.value;
      foundResult.mod = action.payload.mod;
      return { ...state, abilityResults: newAbilityResults };
    default:
      return;
  }
};

export default function GeneratorPage() {
  const [state, dispatch] = useReducer(genReducer, initialGenState);

  useEffect(() => {
    fetch(abilityURL)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        dispatch({ type: "genLoad", payload: data.results });
      });
  }, []);

  return (
    <GenContext.Provider value={[state, dispatch, abilityURL, Form]}>
      <div className="spacing">
        <div className="genHeading">
          <h2 className="firstHeading">Character Generator</h2>
          <p>The character generator can assist you with creating your D&D character! You can select your race, class, and populate your ability scores. From there, the generator will automatically populate the associated skills to their appropriate values. For more information on Ability Scores and Skills, visit the resources page!</p>
        </div>
        <div className="genContainer">
          <Container>
            <Row>
              <Col>
                <RaceSelect />
              </Col>
              <Col>
                <ClassSelect />
              </Col>
            </Row>
          </Container>
          <Container className="abilitySliders">
            <Form>
              <Row>
                {state.abilityResults.map((result) => {
                  return (
                    <Col key={result.index}>
                      <Form.Group key={result.index}>
                        <Form.Label htmlFor={result.index}>
                          {result.name}
                        </Form.Label>
                        <Score result={result} />
                      </Form.Group>
                    </Col>
                  );
                })}
              </Row>
            </Form>
          </Container>
          <Container>
            <Skills />
          </Container>
        </div>
      </div>
    </GenContext.Provider>
  );
}
