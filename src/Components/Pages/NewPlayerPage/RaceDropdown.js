import React, { useContext } from 'react'
import { Dropdown, Accordion } from "react-bootstrap";
import DropdownButton from "react-bootstrap/DropdownButton";
import NewPlayerContext from '../../../Context/NewPlayerContext';

export default function RaceDropdown() {
  const [state, dispatch] = useContext(NewPlayerContext)

  function onSelectRaceSelector(eventKey) {
    state.races.map((choseRace) => {
      if (choseRace.id === eventKey) {
        return dispatch({ type: "racesLoad", payload: choseRace });
      }
      return choseRace;
    });
  }

  return (
    <Accordion.Item eventKey="4">
      <Accordion.Header>
        <h4>A D&D Race is...</h4>
      </Accordion.Header>
      <Accordion.Body>
        <p>
          Your choice of race affects many different aspects of your character. It establishes fundamental qualities that exist throughout your character’s adventuring career. When making this decision, keep in mind the kind of character you want to play. For example, a halfling could be a good choice for a sneaky rogue, a dwarf makes a tough warrior, and an elf can be a master of arcane magic.
        </p>
        <p>
          Your character race not only affects your ability scores and traits but also provides the cues for building your character’s story. Each race’s description in this section includes information to help you roleplay a character of that race, including personality, physical appearance, and features of society. These details are suggestions to help you think about your character; adventurers can deviate widely from the norm for their race. It’s worthwhile to consider why your character is different, as a helpful way to think about your character’s background and personality.
        </p>
        <p>
          To view information about the different races, use the dropdown menu below:
        </p>
        <Dropdown
          onSelect={(eventKey, event) => onSelectRaceSelector(eventKey)}
        >
          <DropdownButton
            id="dropdown-basic-button"
            title="Race Selector"
          >
            {state.races.map(race => {
              return (
                <Dropdown.Item eventKey={race.id} key={race.id}>
                  {race.id}
                </Dropdown.Item>
              )
            })}
          </DropdownButton>
        </Dropdown>
        <h4>{state.currentRace.race}</h4>
        <h5>{state.currentRace.description}</h5>
        <h5>{state.currentRace.subRaces}</h5>
      </Accordion.Body>
    </Accordion.Item>
  )
}
