import React, { useContext } from 'react'
import { Dropdown, Accordion } from "react-bootstrap";
import DropdownButton from "react-bootstrap/DropdownButton";
import NewPlayerContext from '../../../Context/NewPlayerContext';

export default function ClassDropdown() {
  const [state, dispatch] = useContext(NewPlayerContext)

  function onSelectClassSelector(eventKey) {
    state.classes.map((choseClass) => {
      if (choseClass.id === eventKey) {
        return dispatch({ type: "classesLoad", payload: choseClass });
      }
      return choseClass;
    });
  }

  return (
    <Accordion.Item eventKey="3">
      <Accordion.Header>
        <h4>A D&D Class is...</h4>
      </Accordion.Header>
      <Accordion.Body>
        <p>
          Class is the primary definition of what your character can do.
          It’s more than a profession; it’s your character’s calling.
          Class shapes the way you think about the world and interact with
          it and your relationship with other people and powers in the
          multiverse. A fighter, for example, might view the world in
          pragmatic terms of strategy and maneuvering, and see herself as
          just a pawn in a much larger game. A cleric, by contrast, might
          see himself as a willing servant in a god’s unfolding plan or a
          conflict brewing among various deities. While the fighter has
          contacts in a mercenary company or army, the cleric might know a
          number of priests, paladins, and devotees who share his faith.
        </p>
        <p>
          Your class gives you a variety of special features, such as a
          fighter’s mastery of weapons and armor, and a wizard’s spells.
          At low levels, your class gives you only two or three features,
          but as you advance in level you gain more and your existing
          features often improve. Each class entry in this chapter
          includes a table summarizing the benefits you gain at every
          level, and a detailed explanation of each one.
        </p>
        <p>
          To view information about the different classes, use the dropdown menu below:
        </p>
        <Dropdown
          onSelect={(eventKey, event) => onSelectClassSelector(eventKey)}
        >
          <DropdownButton
            id="dropdown-basic-button"
            title="Class Selector"
          >
            {state.classes.map(classes => {
              return (
                <Dropdown.Item eventKey={classes.id} key={classes.id}>
                  {classes.id}
                </Dropdown.Item>
              )
            })}
          </DropdownButton>
        </Dropdown>
        <h4>{state.currentClass.class}</h4>
        <h5>{state.currentClass.description}</h5>
        <h5>{state.currentClass.attribute}</h5>
      </Accordion.Body>
    </Accordion.Item>
  )
}
