import React, { useReducer } from "react";
import { Accordion } from "react-bootstrap";
import ClassesData from "../../Data/ClassesData";
import AttributeData from "../../Data/AttributeData";
import RacesData from "../../Data/RacesData";
import NewPlayerContext from '../../../Context/NewPlayerContext';
import AttributeDropdown from "./AttributeDropdown";
import ClassDropdown from "./ClassDropdown";
import RaceDropdown from "./RaceDropdown";

const initialState = {
  classes: ClassesData,
  currentClass: [],
  races: RacesData,
  currentRace: [],
  attributes: AttributeData,
  currentAttribute: [],
};

const reducer = (state, action) => {
  switch (action.type) {
    case "classesLoad":
      return { ...state, currentClass: action.payload };
    case "attributesLoad":
      return { ...state, currentAttribute: action.payload };
    case "racesLoad":
      return { ...state, currentRace: action.payload };
    default:
      return;
  }
};

export default function NewPlayerPage() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <NewPlayerContext.Provider value={[state, dispatch]}>
      <div className="spacing">
        <section className="newPlayerSection">
          <h2 className='firstHeading'>Welcome New Dungeons & Dragons (D&D) Players!</h2>
          <p>
            This page is an excellent resource for information and links to
            broaden your knowledge and get your D&D journey started on the right
            foot. (Or left foot if you're left-handed. We don't judge.)
          </p>
        </section>
        <section className="newPlayerSection">
          <h2>
            What is D&D?
          </h2>
          <p>
            D&D is commonly recognized as the beginning of modern role-playing
            games.
          </p>
          <p>
            D&D departs from traditional wargaming by allowing each player to
            create their own character to play instead of a military formation.
            These characters embark upon imaginary adventures within a fantasy
            setting. A Dungeon Master (DM) serves as the game's referee and
            storyteller, while maintaining the setting in which the adventures
            occur, and playing the role of the inhabitants of the game world. The
            characters form a party and they interact with the setting's
            inhabitants and each other. Together they solve dilemmas, engage in
            battles, explore, and gather treasure and knowledge. In the process,
            the characters earn experience points (XP) in order to rise in levels,
            and become increasingly powerful over a series of separate gaming
            sessions
          </p>
        </section>
        <Accordion className='accordionNewPlayerPage'>
          <section>
            <Accordion.Item eventKey="1">
              <Accordion.Header>
                <h4>
                  Commonly used D&D terms and definitions:
                </h4>
              </Accordion.Header>
              <Accordion.Body>
                <h4>Dungeon Master (DM)</h4>
                <p>
                  A dungeon master is the person who organizes or directs the
                  story and play in a Dungeons and Dragons game.
                </p>
                <h4>Experience Points (XP)</h4>
                <p>
                  Experience points fuel level advancement for player
                  characters and are most often the reward for completing
                  combat encounters or quests. When adventurers complete a
                  quest and/or defeat one or more monsters - typically by
                  killing, routing, or capturing them - they divide the total
                  XP value of the monsters evenly among themselves.
                </p>
                <h4>Dice Rolls</h4>
                <p>
                  There are multiple kinds of dice with different amounts of
                  faces (e.g. a D20 has 20 sides) and are used to determine
                  the outcome of a decided action. A commonly used term,
                  specifically in reference to a D20 dice, is a 'nat 1' or
                  'nat 20', meaning the dice face landed on a 1 (a critical
                  failure) or 20 (a critical success).
                </p>
              </Accordion.Body>
            </Accordion.Item>
          </section>
          <section>
            <AttributeDropdown />
          </section>
          <section>
            <ClassDropdown />
          </section>
          <section>
            <RaceDropdown />
          </section>
          <section>
            <Accordion.Item eventKey="5">
              <Accordion.Header>
                <h4>
                  Additional considerations:
                </h4>
              </Accordion.Header>
              <Accordion.Body>
                <h4>Personality and Background</h4>
                <p>
                  Characters are defined by much more than their race and class.
                  They’re individuals with their own stories, interests,
                  connections, and capabilities beyond those that class and race
                  define.
                </p>
                <h4>Equipment</h4>
                <p>
                  For an adventurer, the availability of armor, weapons,
                  backpacks, rope, and similar goods is of paramount importance,
                  since proper equipment can mean the difference between life and
                  death in a dungeon or the untamed wilds.
                </p>
              </Accordion.Body>
            </Accordion.Item>
          </section>
        </Accordion>
      </div>
    </NewPlayerContext.Provider>
  );
}