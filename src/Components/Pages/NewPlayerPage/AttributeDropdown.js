import React, { useContext } from 'react'
import { Dropdown, Accordion } from "react-bootstrap";
import DropdownButton from "react-bootstrap/DropdownButton";
import NewPlayerContext from '../../../Context/NewPlayerContext';

export default function AttributeDropdown() {
  const [state, dispatch] = useContext(NewPlayerContext)

  function onSelectAttributeSelector(eventKey) {
    state.attributes.map((choseAttribute) => {
      if (choseAttribute.id === eventKey) {
        dispatch({ type: "attributesLoad", payload: choseAttribute });
      }
      return choseAttribute;
    });
  }

  return (
    <Accordion.Item eventKey="2">
      <Accordion.Header>
        <h4>
          Basic character attributes (or ability scores) used by a D&D Character:
        </h4>
      </Accordion.Header>
      <Accordion.Body>
        <p>
          Is a character muscle-bound and insightful? Brilliant and charming? Nimble and hardy? Ability scores define these qualities--a creature's assets as well as weaknesses. The three main rolls of the game -- the ability check, the saving throw, and the attack roll -- rely on the six ability scores.
        </p>
        <p>
          To view information about the different attributes/ability scores, use the dropdown menu below:
        </p>
        <Dropdown
          onSelect={(eventKey, event) =>
            onSelectAttributeSelector(eventKey)
          }
        >
          <DropdownButton
            id="dropdown-basic-button"
            title="Attribute Selector"
          >
            {state.attributes.map(attribute => {
              return (
                <Dropdown.Item eventKey={attribute.id} key={attribute.id}>
                  {attribute.id}
                </Dropdown.Item>
              )
            })}
          </DropdownButton>
        </Dropdown>
        <h4>{state.currentAttribute.attribute}</h4>
        <h5>{state.currentAttribute.impacts}</h5>
        <h5>{state.currentAttribute.important}</h5>
      </Accordion.Body>
    </Accordion.Item>
  )
}
