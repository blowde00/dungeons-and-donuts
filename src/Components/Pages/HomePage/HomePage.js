import React from 'react';
import { Link } from "react-router-dom";

export default function HomePage() {
  return (
    <div className="spacing homePage">
      <div>
        <img
          className="zoom"
          id="donutWarrior"
          src={require("../../Images/Donut_Warrior.jpg")}
          alt="Donut Warrior"
        />
      </div>
      <section className='homeSection'>
        <h2 className='firstHeading'>
          About the Site
        </h2>
        <span>
          This site was created to assist new players and seasoned veterans with the creation of a new D&D character, as well as providing resources on game mechanics and character attributes.
        </span>
      </section>
      <section className='homeSection'>
        <h2>
        <Link className="homeLink" to='../newplayer'>New Player Page</Link>
        </h2>
        <span>
          This page provides information about D&D rules and requirements to create a new D&D character.  This page is written with new players in mind but could provide useful information to returning players as well.
        </span>
      </section>
      <section className='homeSection'>
        <h2>
        <Link className="homeLink" to='../generator'>Generator Page</Link>
        </h2>
        <span>
          This page is designed to help players (new and old) with generating a new D&D character.
        </span>
      </section >
      <section className='homeSection'>
        <h2>
        <Link className="homeLink" to='../resources'>Resources</Link>
        </h2>
        <span>
          This page contains links and resources to assist players with expanding their knowledge about D&D.
        </span>
      </section>
    </div>
  )
}