import React from 'react';
import { Link } from 'react-router-dom';

export default function Footer() {
  return (
    <div>
      <footer>
        <section>
          <Link className="footerLinks" to='/'>Home</Link>
          <Link className="footerLinks" to='/newplayer'>New Player</Link>
          <Link className="footerLinks" to='/generator'>Character Generator</Link>
          <Link className="footerLinks" to='/resources'>Resources</Link>
        </section>
        <section >
          <span>&#169; Dungeons & Donuts, March 2022</span>
        </section>
      </footer>
    </div>
  )
}
