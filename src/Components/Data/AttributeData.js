let AttributeData = [
  {
    'id': 'Strength',
    'attribute': 'Attribute: Strength',
    'impacts': 'Impacts: Natural athleticism, bodily power',
    'important': 'Important For: Barbarian, Fighter, Paladin'
  },
  {
    'id': 'Dexterity',
    'attribute': 'Attribute: Dexterity',
    'impacts': 'Impacts: Physical agility, reflexes, balance, poise',
    'important': 'Important For: Monk, Ranger, Rogue'
  },
  {
    'id': 'Constitution',
    'attribute': 'Attribute: Constitution',
    'impacts': 'Impacts: Health, stamina, vital force',
    'important': 'Important For: All classes'
  },
  {
    'id': 'Wisdom',
    'attribute': 'Attribute: Wisdom',
    'impacts': 'Impacts: Awareness, intuition, insight',
    'important': 'Important For: Cleric, Druid'
  },
  {
    'id': 'Intelligence',
    'attribute': 'Attribute: Intelligence',
    'impacts': 'Impacts: Mental acuity, information recall, analytical skill',
    'important': 'Important For: Wizard'
  },
  {
    'id': 'Charisma',
    'attribute': 'Attribute: Charisma',
    'impacts': 'Impacts: Confidence, eloquence, leadership',
    'important': 'Important For: Bard, Sorcerer, Warlock'
  } 
]

export default AttributeData;