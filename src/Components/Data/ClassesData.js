let ClassesData = [
  {
    'id': 'Barbarian',
    'class': 'Class: Barbarian',
    'description': 'Description: A fierce warrior of primitive background who can enter a battle rage',
    'attribute': 'Primary Attribute: Strength'
  },
  {
    'id': 'Bard',
    'class': 'Class: Bard',
    'description': 'Description: An inspiring magician whose power echoes the music of creation',
    'attribute': 'Primary Attribute: Charisma'
  },
  {
    'id': 'Cleric',
    'class': 'Class: Cleric',
    'description': 'Description: A priestly champion who wields divine magic in service of a higher power',
    'attribute': 'Primary Attribute: Wisdom'
  },
  {
    'id': 'Druid',
    'class': 'Class: Druid',
    'description': 'Description: A priest of the Old Faith, wielding the powers of nature — moonlight and plant growth, fire and lightning — and adopting animal forms',
    'attribute': 'Primary Attribute: Wisdom'
  },
  {
    'id': 'Fighter',
    'class': 'Class: Fighter',
    'description': 'Description: A master of martial combat, skilled with a variety of weapons and armor',
    'attribute': 'Primary Attribute: Strength or Dexterity'
  },
  {
    'id': 'Monk',
    'class': 'Class: Monk',
    'description': 'Description: A master of martial arts, harnessing the power of the body in pursuit of physical and spiritual perfection',
    'attribute': 'Primary Attribute: Dexterity & Wisdom'
  },
  {
    'id': 'Paladin',
    'class': 'Class: Paladin',
    'description': 'Description: A holy warrior bound to a sacred oath',
    'attribute': 'Primary Attribute: Strength & Charisma'
  },
  {
    'id': 'Ranger',
    'class': 'Class: Ranger',
    'description': 'Description: A warrior who uses martial prowess and nature magic to combat threats on the edges of civilization',
    'attribute': 'Primary Attribute: Dexterity & Wisdom'
  },
  {
    'id': 'Rogue',
    'class': 'Class: Rogue',
    'description': 'Description: A scoundrel who uses stealth and trickery to overcome obstacles and enemies',
    'attribute': 'Primary Attribute: Dexterity'
  },
  {
    'id': 'Sorcerer',
    'class': 'Class: Sorcerer',
    'description': 'Description: A spellcaster who draws on inherent magic from a gift or bloodline',
    'attribute': 'Primary Attribute: Charisma'
  },
  {
    'id': 'Warlock',
    'class': 'Class: Warlock',
    'description': 'Description: A wielder of magic that is derived from a bargain with an extraplanar entity',
    'attribute': 'Primary Attribute: Charisma'
  },
  {
    'id': 'Wizard',
    'class': 'Class: Wizard',
    'description': 'Description: A scholarly magic-user capable of manipulating the structures of reality',
    'attribute': 'Primary Attribute: Intelligence'
  },
]

export default ClassesData;