import React from 'react'
import {Routes, Route} from 'react-router-dom';
import GeneratorPage from './Components/Pages/GeneratorPage/GeneratorPage';
import HomePage from './Components/Pages/HomePage/HomePage';
import NewPlayerPage from './Components/Pages/NewPlayerPage/NewPlayerPage';
import ResourcesPage from './Components/Pages/ResourcesPage/ResourcesPage';
import App from './App';

export default function Router() {
  return (
    <Routes>
    <Route path='/' element={<App/>}>
      <Route path='' element={<HomePage/>}></Route>
      <Route path='newplayer' element={<NewPlayerPage/>}></Route>
      <Route path='generator' element={<GeneratorPage/>}></Route>
      <Route path='resources' element={<ResourcesPage/>}></Route>
    </Route>
  </Routes>

  )
}
