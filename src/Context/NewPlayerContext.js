import React from 'react';

const NewPlayerContext = React.createContext();

export default NewPlayerContext;